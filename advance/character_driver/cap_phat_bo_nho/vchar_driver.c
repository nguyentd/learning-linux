#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/slab.h>
#include "vchar_driver.h"


struct {
	dev_t dev_num;
	struct class *dev_class;
	struct device *dev;
	vchar_dev_t* vchar_hw;
}vchar_drv;

int vchar_init_hw(vchar_dev_t* hw)
{
	char* buf;
	buf = kzalloc(NUM_DEV_REGS*REG_SIZE,GFP_KERNEL);
	if(!buf)
	{
		return -ENOMEM;
	}
	hw->control_regs = buf;
	hw->status_regs = hw->control_regs + NUM_CTRL_REGS;
	hw->data_regs = hw->status_regs + NUM_STS_REGS;
	return 0;
}

void vchar_exit_hw(vchar_dev_t *hw)
{
	kfree(hw->control_regs);
}
int ret;
static int __init char_driver_init(void)
{
    /* cap phat dong device number */

	vchar_drv.vchar_hw = kzalloc(sizeof(vchar_dev_t), GFP_KERNEL);
	if(!vchar_drv.vchar_hw)
	{
		printk("Faile to create datastruct\n");
		device_destroy(vchar_drv.dev_class,vchar_drv.dev_num);
	}

	ret = vchar_init_hw(vchar_drv.vchar_hw);
	if(ret <0)
	{
		printk("initial virtual c failed\n");
		kfree(vchar_drv.vchar_hw);
	}
	printk("initial virtual c successfully\n");
	vchar_drv.dev_num = 0;
    alloc_chrdev_region(&vchar_drv.dev_num, 0, 1, "vchar_dev");

    printk("Insert character driver successfully. major(%d), minor(%d)\n", MAJOR(vchar_drv.dev_num), MINOR(vchar_drv.dev_num));
    vchar_drv.dev_class = class_create(THIS_MODULE, "class_vchar_dev");
    if(vchar_drv.dev_class == NULL)
    {
    	printk("Create device class failed\n");
    	unregister_chrdev_region(vchar_drv.dev_num, 1);
    }
    vchar_drv.dev = device_create(vchar_drv.dev_class,NULL,vchar_drv.dev_num,NULL,"vchar_dev");
    if(IS_ERR(vchar_drv.dev))
    {
    	printk("Create device failed\n");
    	class_destroy(vchar_drv.dev_class);
    }
    return 0;
}

void __exit char_driver_exit(void)
{

	vchar_exit_hw(vchar_drv.vchar_hw);
	kfree(vchar_drv.vchar_hw);
	device_destroy(vchar_drv.dev_class,vchar_drv.dev_num);
	class_destroy(vchar_drv.dev_class);

    unregister_chrdev_region(vchar_drv.dev_num, 1);

    printk("Remove character driver successfully.\n");
}

module_init(char_driver_init);
module_exit(char_driver_exit);

MODULE_LICENSE("GPL");

MODULE_VERSION("1.0");
