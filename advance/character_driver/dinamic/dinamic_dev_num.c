#include <linux/module.h> 
#include <linux/init.h> 
#include<linux/fs.h> 


struct {
	dev_t dev_num;
}vchar_drv;

static int __init char_driver_init(void)
{
    /* cap phat dong device number */
	vchar_drv.dev_num = 0;
    alloc_chrdev_region(&vchar_drv.dev_num, 0, 1, "vchar_dev");

    printk("Insert character driver successfully. major(%d), minor(%d)\n", MAJOR(vchar_drv.dev_num), MINOR(vchar_drv.dev_num));
    return 0;
}

void __exit char_driver_exit(void)
{
    unregister_chrdev_region(vchar_drv.dev_num, 1);

    printk("Remove character driver successfully.\n");
}

module_init(char_driver_init);
module_exit(char_driver_exit);

MODULE_LICENSE("GPL");

MODULE_VERSION("1.0");
