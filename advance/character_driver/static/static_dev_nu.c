#include<linux/module.h>
#include<linux/init.h>
#include<linux/fs.h>


struct _vchar_drv{
	dev_t dev_num;
} vchar_drv;

static int __init drv_init(void){
	vchar_drv.dev_num = MKDEV(400,0);
    register_chrdev_region(vchar_drv.dev_num,1,"static_char_dev");
    printk(KERN_INFO "insert successfully\n");
    return 0;
}

static void __init drv_exit(void){
    unregister_chrdev_region(vchar_drv.dev_num,1);
    printk(KERN_INFO "remove successfully\n");
}

module_init(drv_init);
module_exit(drv_exit);
MODULE_LICENSE("GPL");
MODULE_VERSION("2.1");
MODULE_SUPPORTED_DEVICE("testdevice");
