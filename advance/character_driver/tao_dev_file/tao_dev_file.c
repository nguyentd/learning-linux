#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/device.h>


struct {
	dev_t dev_num;
	struct class *dev_class;
	struct device *dev;
}vchar_drv;

static int __init char_driver_init(void)
{
    /* cap phat dong device number */
	vchar_drv.dev_num = 0;
    alloc_chrdev_region(&vchar_drv.dev_num, 0, 1, "vchar_dev");

    printk("Insert character driver successfully. major(%d), minor(%d)\n", MAJOR(vchar_drv.dev_num), MINOR(vchar_drv.dev_num));
    vchar_drv.dev_class = class_create(THIS_MODULE, "class_vchar_dev");
    if(vchar_drv.dev_class == NULL)
    {
    	printk("Create device class failed\n");
    	unregister_chrdev_region(vchar_drv.dev_num, 1);
    }
    vchar_drv.dev = device_create(vchar_drv.dev_class,NULL,vchar_drv.dev_num,NULL,"vchar_dev");
    if(IS_ERR(vchar_drv.dev))
    {
    	printk("Create device failed\n");
    	class_destroy(vchar_drv.dev_class);
    }
    return 0;
}

void __exit char_driver_exit(void)
{

	device_destroy(vchar_drv.dev_class,vchar_drv.dev_num);
	class_destroy(vchar_drv.dev_class);

    unregister_chrdev_region(vchar_drv.dev_num, 1);

    printk("Remove character driver successfully.\n");
}

module_init(char_driver_init);
module_exit(char_driver_exit);

MODULE_LICENSE("GPL");

MODULE_VERSION("1.0");
