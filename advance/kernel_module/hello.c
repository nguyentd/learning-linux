#include <linux/module.h>

#define DRIVER_DESC   "hello kernel module"


static int __init init_hello(void)
{
    printk("Start Hello Module\n");
    return 0;
}

static void __exit exit_hello(void)
{
    printk("Exit Hello Module\n");
}

module_init(init_hello);
module_exit(exit_hello);

MODULE_LICENSE("GPL"); 
MODULE_DESCRIPTION(DRIVER_DESC); 
