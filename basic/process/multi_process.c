#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(){
    pid_t child_pid;
    int count = 2;

    printf("Count khoi tao : %d\n",count);
    child_pid = fork();

    if(child_pid >= 0){
        if(0==child_pid){
            count++;
            sleep(2);
            printf("chuong trinh con, sau khi tang: %d\n",count);
            //while(1);
        }else{
            count++;
            sleep(5);
            printf("chuong trinh cha: %d\n",count);
            //while(1);
        }

    }else{
        printf("tao process ko thanh cong\n");
    } 

    return 0;
}